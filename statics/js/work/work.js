let balance = 0

const deposit = (amount) => {
    balance += amount
}

const withdraw = (amount) => {
    balance -= amount
}

const getBalance = () => {
    return balance
}


const bank = {
    deposit,
    withdraw,
    getBalance
}

export default bank