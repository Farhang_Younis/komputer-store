import fetchLaptops from "./api/laptops.js"
import laptopsView from "./laptops/laptopsView.js"
import bank from "./bank/bank.js"
import work from "./work/work.js"

const laptopsSelectElement = document.getElementById("laptops")
const featuresElement = document.getElementById("features")
const titleElement = document.getElementById("title")
const descriptionElement = document.getElementById("description")
const priceElement = document.getElementById("price")
const balanceElement = document.getElementById("balance")
const loanBalanceElement = document.getElementById("loanBalance")
const loanContainerElement = document.getElementById("loanContainer")
const payElement = document.getElementById("pay")
const repayLoanElement = document.getElementById("repayLoan")
const btnLoanElement = document.getElementById("btnLoan")
const btnBankElement = document.getElementById("btnBank")
const btnWorkElement = document.getElementById("btnWork")
const btnRepayLoanElement = document.getElementById("btnRepayLoan")
const imgElement = document.getElementById("laptopImage")
const btnBuyElement = document.getElementById("btnBuy")

function formatCurrency(currence) {
    return new Intl.NumberFormat('dk-DK', { style: 'currency', currency: 'DKK' }).format(currence)
}

function displayOfLoan() {
    const outstandingLoan = bank.getLoan()
    loanBalanceElement.innerText=formatCurrency(outstandingLoan)    

    if (outstandingLoan < 1) {
        loanContainerElement.style.display = 'none'
    } else {
        loanContainerElement.style.display = 'block'
    }    
}

function displayOfRepayLoan() {
    const outstandingLoan = bank.getLoan()

    if (outstandingLoan < 1) {
        repayLoanElement.style.display = 'none'
    } else {
        repayLoanElement.style.display = 'block'
    }    
}

function displayOfLoanAndPayLoan() {
    displayOfRepayLoan()
    displayOfLoan()
}
// Fetching Initial Data
const initialLaptops = await fetchLaptops()
//laptopsView.setLaptops(initialLaptops)

balanceElement.innerText = formatCurrency(bank.getBalance())
payElement.innerText = formatCurrency(work.getBalance())
displayOfLoanAndPayLoan()
imgElement.style.display="none"

//Population af select element with laptops
for (const laptop of initialLaptops) {
    const optionElement = document.createElement("option")
    optionElement.value = laptop.id
    optionElement.text = laptop.title
    laptopsSelectElement.appendChild(optionElement)
}

laptopsSelectElement.addEventListener("change", (e) => {
    
    const laptop = initialLaptops[e.target.selectedIndex]
    const specs =laptop.specs
    //featuresElement.innerText = laptop.specs
    
    if (featuresElement.hasChildNodes()) {
        featuresElement.innerHTML=''
    }
    
    specs.forEach(element => {
        const listElement = document.createElement("li")
        listElement.innerText=element;
        featuresElement.appendChild(listElement)

    });
    titleElement.innerText = laptop.title
    descriptionElement.innerText = laptop.description
    priceElement.innerText = formatCurrency(laptop.price)
    const imageUrl = "https://hickory-quilled-actress.glitch.me/"
       
    imgElement.src= `${imageUrl}${laptop.image}`
    imgElement.style.display="block"    
})

btnLoanElement.addEventListener("click", () => {

    const balance = bank.getBalance()
    const outStandingLoan = bank.getLoan()
    // Calculates the allowed amount to be loaned
    const maxLoan = parseInt((balance * 2), 10)

    // Checks if there is enough money in the account/balance to take a loan based on
    if (balance > 0) {
        let input = prompt("Please enter the amount of loan");

        if (!isNaN(input)) {
            const amountToLoan = Number(input)

            // Checks if loan is paid in full 
            if (outStandingLoan < 1) {
                // Checks the amount to be loand is in the range 
                if (amountToLoan <= maxLoan) {
                    bank.depositLoan(amountToLoan)
                    displayOfLoanAndPayLoan()
                } else {
                    alert(`Sorry, The Amount ${amountToLoan} Is Too High!`)
                }
            } else {
                alert(`Sorry, The Loan ${outStandingLoan} Should Be Payed in Full Before Taking a New Loan!`)
            }

        } else {
            alert("Please Enter a Number");
        }

    } else {
        alert("Sorry, Your Balance Is 0!");
    }
})

btnBankElement.addEventListener("click", () => {
    const workBalance = work.getBalance()
    const loan = bank.getLoan()
    const amount = (workBalance * (10 / 100))

    if (loan > 0) {
        // Deduction of loan and work balance by 10%
        work.withdraw(amount)
        bank.withdrawLoan(amount)

        //Transfering work balance after deduction to bank balance
        const newWorkBalance = work.getBalance()
        work.withdraw(newWorkBalance)
        bank.deposit(newWorkBalance)

    } else {
        work.withdraw(workBalance)
        bank.deposit(workBalance)
    }

    balanceElement.innerText = formatCurrency(bank.getBalance())
    payElement.innerText = formatCurrency(work.getBalance())
    displayOfLoanAndPayLoan()

})

btnWorkElement.addEventListener("click", () => {
    work.deposit(100)
    payElement.innerText = formatCurrency(work.getBalance())
})

btnRepayLoanElement.addEventListener("click", () => {
    const workBalance = work.getBalance()
    const outstandingLoan = bank.getLoan()
    const amount = workBalance - outstandingLoan

    if (workBalance > 0) {

        console.log(`workBalance ${workBalance} outstandingLoan ${outstandingLoan} amount ${amount}`)
        bank.withdrawLoan(outstandingLoan)
        work.withdraw(outstandingLoan)

        const newWorkBalance = work.getBalance()
        if (newWorkBalance > 0) {
            bank.deposit(newWorkBalance)
            work.withdraw(newWorkBalance)
        }
        displayOfLoanAndPayLoan()
        balanceElement.innerText=bank.getBalance()
    }

    payElement.innerText = formatCurrency(work.getBalance())
})

btnBuyElement.addEventListener("click", () => {
    const balance = bank.getBalance()
    const laptop = initialLaptops[laptopsSelectElement.selectedIndex]

    if (balance > 0 && balance >=laptop.price) {        
        bank.withdraw(laptop.price)
        alert("You Are Now Owner Of The Laptop!")
    }else{
        alert("Sorry, You Can't Afford the Laptop!")
    }

    balanceElement.innerText = formatCurrency(bank.getBalance())
})

imgElement.addEventListener("error",()=>{
    imgElement.style.display="none"
})