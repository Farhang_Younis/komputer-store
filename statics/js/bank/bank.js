let balance = 0
let loan=0

const deposit = (amount) => {
    balance += amount
}

const withdraw = (amount) => {
    balance -= amount
}

const getBalance = () => {
    return balance
}

const depositLoan = (amount) => {
    loan += amount
}

const withdrawLoan = (amount) => {
    loan -= amount
}

const getLoan = () => {
    return loan
}

const bank = {
    deposit,
    withdraw,
    getBalance,
    depositLoan,
    withdrawLoan,
    getLoan
}

export default bank