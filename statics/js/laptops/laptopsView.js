let currentLaptops = []

const getLaptops = () => [...currentLaptops]

const setLaptops = (laptops) => currentLaptops = laptops

const laptopsView = {
    getLaptops,
    setLaptops    
}

export default laptopsView  